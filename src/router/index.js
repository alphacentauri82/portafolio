import Vue from 'vue'
import Router from 'vue-router'
import Profile from '@/components/Profile'
import Resume from '@/components/Resume'
import Talks from '@/components/Talks'
import MyHistory from '@/components/MyHistory'
import Projects from '@/components/Projects'
import Events from '@/components/Events'
import Contact from '@/components/Contact'
import Footer from '@/components/Footer'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/Profile'
    },
    {
      path: '*',
      redirect: '/Profile'
    },
    {
      path: '/Profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/Resume',
      name: 'Resume',
      component: Resume
    },
    {
      path: '/MyHistory',
      name: 'MyHistory',
      component: MyHistory
    },
    {
      path: '/Projects',
      name: 'Projects',
      component: Projects
    },
    {
      path: '/Events',
      name: 'Events',
      component: Events
    },
    {
      path: '/Talks',
      name: 'Talks',
      component: Talks
    },
    {
      path: '/Contact',
      name: 'Contact',
      component: Contact
    }
    
  ]
})
